
Summary & Notes
=========================

Given a list of rectangles (or rotated rectangles or polygons) and a corresponding list of scores (confidences),
the Non Maximal Suppression functions below will return a list of indicies.  These indicies correspond to the rectangles
(or rotated rectangles or polygons) that have the highest scores and the minimum overlap with other elements in the
list.

Rectangles (sometimes referred to as rects and boxes) are expressed as a 4-tuple: (x, y, w, h). Where x, y is the coordinate
of the upper left hand corner of the rectangle.  w and h are the width and height of the rectangle.

Rotated rectangles (sometimes referred to rrects or rboxes) are expressed as a 5-tuple: (x, y, w, h, r). Where the
first four members are as described for rectangles and r is the angle, in radians, of rotation around the center of
the rectangle.

Polygons are expressed as a list of 2-tuples of (x, y).  Each x, y describes a verticie of the polygon.  The edges
of the polygon are described by each pair of consecutive vertices.  That is (x0, y0), (x1, y1) describe and edge of
the polygon.  The polygons are closed by the edge (xn, yn), (x0, y0).

If you only have a need for NMS on rectangles and are already using OpenCV, you may want to consider using
`cv2.dnn.NMSBoxes <https://docs.opencv.org/master/d6/d0f/group__dnn.html#ga9d118d70a1659af729d01b10233213ee>`_.
However, you might see a small performance gain using nms over NMSBoxes -- nms defaults to using the 
Malisiiewicz et. al. algorithm vs. the Fast algorithm in NMSBoxes.  

PSA: It's not well documented that the bboxes parameter for NMSBoxes is a numpy array of ((x, y), (w, h)).

nms was developed with Python 3.7 and OpenCV 4.0.0-pre.  It's very likely that nms will work just fine with early
versions of OpenCV.

This code is beta and lacking in test coverage (read: no coverage)

Install
=========================

nms has dependencies on cv2 (OpenCV 4.0.0-pre), numpy and math.  `Adrian Rosenbrock has excellent instructions for
installing OpenCV 4.0 on MacOS and Ubuntu <https://www.pyimagesearch.com/2018/08/17/install-opencv-4-on-macos/>`_

Include nms in your requirements.txt or install into your environment::

  pip install nms

Basic Usage
=========================

Given a list of rotated rectangles *rrects* and confidences *scores*::

    indicies = nms.nms.nms_rboxes(rrects, scores)

By default, :func:`nms.nms.nms_boxes`, :func:`nms.nms.nms_polygons` and :func:`nms.nms.nms_rboxes` use the Malisiiewicz
et. al. NMS Algorithm.  If you would prefer to use the OpenCV2 Fast algorithm or Felzenszwalb et. al. algorithm you
can do so with the optional parameter *nms_algorithm*.  For example::

    indicies = nms.nms.nms_rboxes(rrects, scores, nms_algorithm=malisiewicz.nms)

:func:`nms.nms.nms_boxes`, :func:`nms.nms.nms_polygons` and :func:`nms.nms.nms_rboxes` will also accept a list of
key word arguments (\*\*kwargs).  The kwargs are passed to the NMS function to set values for the NMS threshold,
confidence threshold, top_k and eta (eta is only applicable when using Fast).

For example, to set the NMS threshold to 0.5 (note that use of kwargs requires that nms_algorithm parameter be present)::

    indicies = nms.nms.nms_rboxes(rrects, scores, nms_algorithm=malisiewicz.nms, nms_threshold=0.5)

Please see the docs for :func:`nms.fast.nms`, :func:`nms.malisiewicz.nms` and :func:`nms.felzenszwalb.nms` for
additional information on kwargs

Example
=========================

For a simple example of using nms with geometry returned from EAST, take a look at
`opencv-text-detection <https://bitbucket.org/tomhoag/opencv-text-detection>`_.


Most Useful Functions
=========================

.. automodule:: nms.nms
   :noindex:
   :members:
