nms package
===========

Submodules
----------

nms.fast module
---------------

.. automodule:: nms.fast
    :members:
    :undoc-members:
    :show-inheritance:

nms.felzenszwalb module
-----------------------

.. automodule:: nms.felzenszwalb
    :members:
    :undoc-members:
    :show-inheritance:

nms.helpers module
------------------

.. automodule:: nms.helpers
    :members:
    :undoc-members:
    :show-inheritance:

nms.malisiewicz module
----------------------

.. automodule:: nms.malisiewicz
    :members:
    :undoc-members:
    :show-inheritance:

nms.nms module
--------------

.. automodule:: nms.nms
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: nms
    :members:
    :undoc-members:
    :show-inheritance:
