.. nms documentation master file, created by
   sphinx-quickstart on Wed Aug 29 14:11:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

nms documentation
===============================

.. toctree::
   code
   source/modules
   genindex
   :maxdepth: 2
