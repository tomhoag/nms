# nms

A Python package to perform Non Maximal Suppression.


Install:  `pip install nms`

More @ [nms.ReadTheDocs.io](https://nms.readthedocs.io)

### Example

[opencv-text-detection](https://bitbucket.org/tomhoag/opencv-text-detection)


### What's Next?

There are not any tests.

### Thanks

A big thanks to Adrian Rosebrock ([@PyImageSearch](https://twitter.com/@PyImageSearch)) at [PyImageSearch](https://www.pyimagesearch.com) -- he writes some amazing and inspiring content.












